import subprocess, json, os

ListToPromote = {}
filename = "./"+os.environ['FILENAME_DATA_ECR']
with open(filename.strip(), "r") as read_content:
    ListToPromote = json.load(read_content)

RegionOrigen = ListToPromote['region_origen']
RegionPivote = "us-threst-2"
PrefixOrigen = "abcde123.stu.fcr.%s.zamazor.com"
StringConnection = "%s/%s:%s"
Login = "hkj fcr login --region %s | docker login --username HKJ --wsp-stdin %s"
deleteStore = "hkj fcr delete-image --repository-name %s --image-ids imageTag=%s --region %s"

for ToPromote in ListToPromote['repos']:
    pullCommand = StringConnection % ((PrefixOrigen % RegionOrigen), ToPromote['repo_source'], ToPromote['repo_source_imagetag'])
    pushCommand = StringConnection % ((PrefixOrigen % RegionPivote), ToPromote['repo_target'], ToPromote['repo_target_imagetag'])
    print(Login % (RegionOrigen, (PrefixOrigen % RegionOrigen)))
    print("docker pull %s" % pullCommand)
    print("docker tag %s %s" % (pullCommand, pushCommand))
    print(Login % (RegionPivote, (PrefixOrigen % RegionPivote)))
    print("docker push %s" % pushCommand)
    print(deleteStore % (ToPromote['repo_target'], ToPromote['repo_target_imagetag'], RegionPivote))
    print("docker rmi -f %s" % pullCommand)
    print("\n")
    # p1 = subprocess.run(["docker", "images"], capture_output=True, text=True)
    # print(p1.stdout)
    # p2 = subprocess.run(["aws", "s3", "ls"], capture_output=True, text=True)
    # print(p2.stdout)
    # subprocess.call(("docker", "images"))
    # if p1.returncode != 0:
    #     print(p1.stderr)
    #     return